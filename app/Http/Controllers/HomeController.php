<?php

namespace App\Http\Controllers;

use App\Models\Coin;
use App\Models\CoinPrice;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{


    public function index()
    {
        
        $list = $this->getCoins();
        //return view('welcome2', compact('list'));

        return view('welcome', compact('list'));
    }

    public function getCoins()
    {
        // $coin_price = Coin::select(['name'])->withMax('coinPrices as last_price', 'id')->get();

        // $list = [];


        // foreach ($coin_price as $item) {
        //     $price = CoinPrice::find($item->last_price)->price;

        //     $list[$item->name] = $price;
        // }



        $list = Coin::join('coin_prices', 'coins.id', '=', 'coin_prices.coin_id')
            ->latest()
            ->take(10)
            ->orderBy('coins.id')
            ->get(['name', 'price', 'created_at']);


        return $list;
    }
}
