<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        $coins=$this-> getCoins();
        foreach ($coins as $key => $value) {
            dd($value);
        }

        dd($coins);

    }

    public function getCoins()
    {
        $coins=[
            ['name'=>'Bitcoin','symbol'=>'BTC','logo_path'=>'Bitcoin'],
            ['name'=>'Ethereum','symbol'=>'ETH','logo_path'=>'Ethereum'],
            ['name'=>'Tether','symbol'=>'USDT','logo_path'=>'Tether'],
            ['name'=>'BNB','symbol'=>'BNB','logo_path'=>'BNB'],
            ['name'=>'USD Coin','symbol'=>'USDC','logo_path'=>'USD Coin'],
            ['name'=>'XRP','symbol'=>'XRP','logo_path'=>'XRP'],
            ['name'=>'Terra','symbol'=>'LUNA','logo_path'=>'Terra'],
            ['name'=>'Cardano','symbol'=>'ADA','logo_path'=>'Cardano'],
            ['name'=>'Solana','symbol'=>'SOL','logo_path'=>'Solana'],
            ['name'=>'Avalanche','symbol'=>'AVAX','logo_path'=>'Avalanche']
        ];

        return $coins;


    }

}
