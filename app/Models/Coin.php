<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function coinPrices()
    {
        return $this->hasMany(CoinPrice::class);
    }

    // public function latestCoinPrices()
    // {
    //     return $this->hasOne(CoinPrice::class)->latest();
    // }
}
