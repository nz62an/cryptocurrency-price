<?php

namespace App\Models;

use App\Models\CoinPair;
use App\Models\Exchange;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderBook extends Model
{
    use HasFactory;

    protected $guarded=[];

    public function exchange(){
        return $this->belongsTo(Exchange::class);
    }

    public function coinPair(){
        return $this->belongsTo(CoinPair::class);
    }
}
