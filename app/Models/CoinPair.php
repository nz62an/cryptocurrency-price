<?php

namespace App\Models;

use App\Models\OrderBook;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CoinPair extends Model
{
    use HasFactory;

    public function orderBooks(){
        return $this->hasMany(OrderBook::class);
    }
}
