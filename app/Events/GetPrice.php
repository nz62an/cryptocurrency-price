<?php

namespace App\Events;

use App;
use Lin\Binance\BinanceWebSocket;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;
use Lin\Bitmex\BitmexWebSocket;

class GetPrice implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $crypto_prices;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($crypto_prices)
    {
        $this->crypto_prices = $crypto_prices;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //$this->getPriceOfConinsFromBinance();


        // foreach ($coin_prices as $coin_name => $price) {

        //     $coin = Coin::query()->where('name', $coin_name)->first();

        //     $coin->coinPrices()->create([
        //         'price' => $price,
        //     ]);
        // }


        return new Channel('crypto-price');
    }

    public function broadcastAs()
    {
        return 'GetPriceCryptoEvent';
    }

    public function getPriceOfConinsFromBinance()
    {
    }
}
