<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        switch (config('exchanges.default')) {
            case 'Coinex':
                // send coin price every 3 seconds
                $seconds = 3;

                $schedule->call(function () use ($seconds) {

                    $dt = Carbon::now();

                    $x = 60 / $seconds;

                    do {
                        Artisan::call('coinex:getprice');

                        time_sleep_until($dt->addSeconds($seconds)->timestamp);
                    } while (--$x > 0);
                })->everyMinute()
                    ->withoutOverlapping();

                break;
            case 'Kucoin':
                # code...
                break;
            case 'Binance':
                # code...
                break;
            case 'Bitmex':
                print_r('schedule run....');
                Artisan::call('coin:getprice');
                print_r('schedule end....');
                break;
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {

        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return 'Asia/Tehran';
    }
}
