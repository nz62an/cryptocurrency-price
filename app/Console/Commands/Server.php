<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Lin\Bitmex\BitmexWebSocket;

class Server extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server {start}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run websocket server and client bitmex';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bitmex = new BitmexWebSocket();

        $bitmex->config([
            //Do you want to enable local logging,default false
            //'log'=>true,
            //Or set the log name
            'log' => ['filename' => 'bitmex'],

            //Daemons address and port,default 0.0.0.0:2211
            //'global'=>'127.0.0.1:2211',

            //Channel subscription monitoring time,2 seconds
            //'listen_time'=>2,

            //Channel data update time,default 0.5 seconds
            //'data_time'=>0.5,

            //Heartbeat time,default 30 seconds
            //'ping_time'=>30,

            //Number of messages WS queue shuold hold, default 100
            //'queue_count'=>100,

            //baseurl host
            //'baseurl'=>'ws://www.bitmex.com/realtime',//default
            //'baseurl'=>'ws://testnet.bitmex.com/realtime',//test
        ]);

        $bitmex->start();



    }
}

