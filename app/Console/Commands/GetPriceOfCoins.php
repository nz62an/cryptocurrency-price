<?php

namespace App\Console\Commands;

use Goutte\Client;
use App\Events\GetPrice;
use App\Models\Exchange;
use App\Models\OrderBook;
use Illuminate\Console\Command;
use Lin\Bitmex\BitmexWebSocket;
use Illuminate\Support\Facades\Log;


class GetPriceOfCoins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coin:getprice';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Price Of Coins From Binance WebSocket.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(BitmexWebSocket $bitmex)
    {
        $bitmex->config([
            ///Do you want to enable local logging,default false
            //'log'=>true,
            //Or set the log name
            'log' => ['filename' => 'bitmex'],

            //Daemons address and port,default 0.0.0.0:2216
            //'global'=>'127.0.0.1:2216',

            //Channel subscription monitoring time,2 seconds
            //'listen_time'=>2,

            //Channel data update time,default 0.5 seconds
            //'data_time'=>0.5,

            //Heartbeat time,default 30 seconds
            //'ping_time'=>30,

            //Number of messages WS queue shuold hold, default 100
            //'queue_count'=>100,

            //私有数据队列默认保存100条
            //'queue_count'=>100,

            //baseurl host
            //'baseurl'=>'ws://www.bitmex.com/realtime',//default
            //'baseurl'=>'ws://testnet.bitmex.com/realtime',//test
        ]);

        $bitmex->keysecret([
            'key' => 'N9Pibnqx97WM3miSpeUFFaPb',
            'secret' => 'g_wkwKcRO4pYgau-sBLMu7n5CKYtCQ2TKAwV7NE3TxIr3tMW',
        ]);
        $bitmex->subscribe([
            //public
            //'instrument:XBTUSD',
            'orderBook10:XBTUSD',
            // 'quoteBin5m:XBTUSD',

            //private
            // "affiliate",
            // "execution",
            // "order",
            // "margin",
            // "position",
            // "privateNotifications",
            // "transact",
            // "wallet"
        ]);


        // dont work show php your file <command>  [mode] start stop status  worker
        //The third way is to guard the process
        // $bitmex->getSubscribe([
        //     //'instrument:XBTUSD',
        //     'orderBook10:XBTUSD',
        // ], function ($data) {
        //     print_r(json_encode($data));
        // },true);



        while (1) {
            pcntl_alarm(0);
            sleep(1);
            $data = $bitmex->getSubscribes();
            if (isset($data)) {
                # code...
            }



            // print_r(data_get($data,'orderBook10:XBTUSD.data.0.asks'));
            print_r($data);
            // print_r('-----1-----');
            // print_r($data['orderBook10:XBTUSD']['data'][0]['asks']);
            // print_r('-----2-----');
            // print_r(array_column($data['orderBook10:XBTUSD']['data'][0]['asks'], 0));

            // //Bitmex Exchange id equal 4

            // print_r('-----3-----');
            $asks = $data['orderBook10:XBTUSD']['data'][0]['asks'];
            foreach ($asks as $ask) {
                OrderBook::create([
                    'market' => 'XBTUSD',
                    'price' => $ask[0],
                    'size'=>$ask[1],
                    'order_type'=>'sell',
                    'exchanges_id'=>4
                ]);
            }

            $bids = $data['orderBook10:XBTUSD']['data'][0]['bids'];
            foreach ($bids as $bid) {
                OrderBook::create([
                    'market' => 'XBTUSD',
                    'price' => $bid[0],
                    'size'=>$bid[1],
                    'order_type'=>'buy',
                    'exchanges_id'=>4
                ]);
            }

            //$bids=$data['orderBook10:XBTUSD']['data'][0]['bids'];

            event(new GetPrice([data_get($data,'orderBook10:XBTUSD.data.0.bids'),data_get($data,'orderBook10:XBTUSD.data.0.asks')]));
        }
    }


    public function getCryptoPricesFromBitmex()
    {






        //return data_get($data,'orderBook10:XBTUSD.data.0.bids');

        // $bitmex->subscribe([
        //     //public
        //     'orderBook10:XBTUSD',
        //     // 'quoteBin5m:XBTUSD',

        //     //private
        //     // "affiliate",
        //     // "execution",
        //     // "order",
        //     // "margin",
        //     // "position",
        //     // "privateNotifications",
        //     // "transact",
        //     // "wallet"
        // ]);

        // sleep(5);


        // print_r(data_get($data,'orderBook10:XBTUSD.data'));
        // print_r(data_get($data,'orderBook10:XBTUSD.data.0.bids'));

        // Log::info('get bitmex crypto prices');
        // Log::info(data_get($data,'orderBook10:XBTUSD.data'));
    }

    function requsetToCoinmarketcap()
    {

        $client = new Client();
        //success or faile timeout 3 times
        $crawler = $client->request('GET', 'https://coinmarketcap.com/coins/');

        $coin_price = null;
        for ($i = 1; $i <= 10; $i++) {


            $coin_name = $crawler->filter("table>tbody>tr:nth-child($i)>td:nth-child(3)>div>a>div>div>p")->text();


            $price = $crawler->filter("table>tbody>tr:nth-child($i)>td:nth-child(4)")->text();

            $coin_price[$coin_name] = $price;
        }


        return $coin_price;
    }
}
