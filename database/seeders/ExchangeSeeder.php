<?php

namespace Database\Seeders;

use App\Models\Exchange;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ExchangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exchanges=[
            ['name'=>'Coinex'],
            ['name'=>'Kucoin'],
            ['name'=>'Binance'],
            ['name'=>'Bitmex']
        ];

        Exchange::insert($exchanges);


    }
}
