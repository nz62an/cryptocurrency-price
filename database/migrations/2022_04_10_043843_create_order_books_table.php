<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_books', function (Blueprint $table) {
            $table->id();

            // $table->unsignedBigInteger('coin_pairs_id')->default(1);
            // $table->foreign('coin_pairs_id')->references('id')->on('coin_pairs');

            $table->string('market');

            $table->string('price');
            $table->string('size');
            $table->string('order_type');

            $table->unsignedBigInteger('exchanges_id')->default(1);
            $table->foreign('exchanges_id')->references('id')->on('exchanges');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_books');
    }
};
