<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel Broadcast Redis Socket io Tutorial - CodeCheef.org</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">

    {{-- <style>
        .bids td {
            display: inline-block;
        }

    </style> --}}

</head>

<body>



    <div class="container mt-4 ml-0 mr-0">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col">
                <div class="card border-success">
                    <div class="card-header bg-success  text-light text-end">
                        پیشنهادهای خرید
                    </div>
                    <div class="card-body">
                        <table class="table bids" style="width: 100%">
                            <thead>
                                <tr>
                                    <th scope="col">قیمت</th>
                                    <th scope="col">مقدار</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card border-danger">
                    <div class="card-header bg-danger text-light text-end">
                        پیشنهادهای فروش
                    </div>
                    <div class="card-body">
                        <table class="table asks text-center" style="width: 100%">
                            <thead>
                                <tr>
                                    <th scope="col">قیمت</th>
                                    <th scope="col">مقدار</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="{{ mix('js/app.js') }}"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>


</body>

<script>
    window.laravel_echo_port = {{ env('LARAVEL_ECHO_PORT') }};
</script>
<script src="http://{{ Request::getHost() }}:{{ env('LARAVEL_ECHO_PORT') }}/socket.io/socket.io.js"></script>
{{-- <script src="http://{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script> --}}
<script src="{{ url('/js/laravel-echo-setup.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    window.Echo.channel('crypto-price')
        .listen('.GetPriceCryptoEvent', (data) => {

            console.log(data);
            var sum;


            $('.bids').DataTable({
                destroy: true,
                data: data.crypto_prices[0],
                "language": {
                    "decimal": ".",
                    "thousands": ","
                },
                columns: [{
                        title: "قیمت"
                    },
                    {
                        title: "مقدار"
                    },
                ],
                "drawCallback": function(settings) {


                    var table = $('.bids').DataTable();

                    // var api=this.api();
                    // // Output the data for the visible rows to the browser's console
                    // console.log('aa', api.column(1).data());

                    sum = table.column(1)
                        .data()
                        .reduce(function(a, b) {
                            return a + b;
                        });
                    console.log('sum: ', sum);



                    var col1 = "#CEDECD",
                        col2 = "#fff";


                    console.log(table.rows({
                        page: 'current'
                    }).data());

                    table.rows().every(function(rowIdx, tableLoop, rowLoop) {
                        console.log(rowIdx, ' ', this.data()[1]);
                        var percentage = this.data()[1] / sum*100;
                        console.log('% :', percentage);
                        table.row(rowIdx).node().style.background =
                            "-webkit-gradient(linear, left top,right top, color-stop(" +
                            percentage +
                            "%," + col1 + "), color-stop(" + percentage + "%," + col2 + "))";
                        table.row(rowIdx).node().style.background =
                            "-moz-linear-gradient(left center," + col1 +
                            " " +
                            percentage + "%, " + col2 + " " + percentage + "%)";
                        table.row(rowIdx).node().style.background = "-o-linear-gradient(left," + col1 +
                            " " +
                            percentage +
                            "%, " + col2 + " " + percentage + "%)";
                        table.row(rowIdx).node().style.background = "linear-gradient(to right," +
                            col1 + " " +
                            percentage +
                            "%, " + col2 + " " + percentage + "%)";

                    });



                }
            });
            $('.asks').DataTable({
                destroy: true,
                data: data.crypto_prices[1],
                "language": {
                    "decimal": ".",
                    "thousands": ","
                },
                columns: [{
                        title: "قیمت"
                    },
                    {
                        title: "مقدار"
                    },
                ],
                "drawCallback": function(settings) {


                    var table = $('.asks').DataTable();

                    // var api=this.api();
                    // // Output the data for the visible rows to the browser's console
                    // console.log('aa', api.column(1).data());

                    sum = table.column(1)
                        .data()
                        .reduce(function(a, b) {
                            return a + b;
                        });
                    console.log('sum: ', sum);



                    var col1 = "#D6C9CB",
                        col2 = "#fff";


                    console.log(table.rows({
                        page: 'current'
                    }).data());

                    table.rows().every(function(rowIdx, tableLoop, rowLoop) {
                        console.log(rowIdx, ' ', this.data()[1]);
                        var percentage = this.data()[1] / sum*100;
                        console.log('% :', percentage);
                        table.row(rowIdx).node().style.background =
                            "-webkit-gradient(linear, left top,right top, color-stop(" +
                            percentage +
                            "%," + col1 + "), color-stop(" + percentage + "%," + col2 + "))";
                        table.row(rowIdx).node().style.background =
                            "-moz-linear-gradient(left center," + col1 +
                            " " +
                            percentage + "%, " + col2 + " " + percentage + "%)";
                        table.row(rowIdx).node().style.background = "-o-linear-gradient(left," + col1 +
                            " " +
                            percentage +
                            "%, " + col2 + " " + percentage + "%)";
                        table.row(rowIdx).node().style.background = "linear-gradient(to right," +
                            col1 + " " +
                            percentage +
                            "%, " + col2 + " " + percentage + "%)";

                    });



                }
            });

            /*  var coinTable = document.getElementById('coinTable');
            var table = `<table id="coinTable" class="table">
            <thead>
            <tr>
                <th data-field="name" scope="col">Name</th>
                <th data-field="price" scope="col">Price</th>
            </tr>
            </thead>
            <tbody>`;

            for (let i = 0; i < Object.keys(data).length - 1; i++) {

                // console.log(data[i]['name']);
                table += ` <tr>

        <th scope="row"><img class="coin-logo" src="/images/${data[i]['name']}.png" alt=""> ${data[i]['name']}</th>
        <td>${data[i]['price']}</td>
        <td>${data[i]['created_at']}</td>

    </tr>`;
            }

            table += ` </tbody>
                       </table>`;
            coinTable.outerHTML = table; */

        });
</script>

</html>
