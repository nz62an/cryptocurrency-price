<div class="alert alert-{{$type}}">
    <!-- I begin to speak only when I am certain what I will say is not better left unsaid. - Cato the Younger -->

    {{$message}}
</div>
