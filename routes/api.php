<?php


use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/coins',[HomeController::class,'getCoins']);

Route::middleware(['throttle:ip_address'])->group(function () {
    Route::get('/api/coins', [HomeController::class, 'getCoins']);
});
